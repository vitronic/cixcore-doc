=========
Provision
=========

cosas


Perfiles
--------

.. image:: ./imgs/profiles.png


Servicios
---------

.. image:: ./imgs/services.png


Templates
---------



Lista Blanca
------------

La Lista Blanca de CixCore permite mantener un registro de los clientes

.. image:: ./imgs/whitelist/whitelist.png

¿Cómo puedo registrar una nueva Lista Blanca?
---------------------------------------------

.. image:: ./imgs/whitelist/whitelist_new_button.png

Dirijase al menú **PROVISIÓN > Lista Blanca** a continuación pulse el botón **+** en la esquina 
superior derecha (Mostrar pantallaso), esto le permitira acceder al formulario de registros de 
una nueva Lista Blanca, a continuación detallaremos los campos del formulario:

.. image:: ./imgs/whitelist/whitelist_new_form.png

* **Nombre de la lista**: Se trata de una entrada donde se debe definir el nombre de la Lista Blanca.

* **Fecha vencimiento**: Aquí deberá ser seleccionada la fecha en la que la Lista Blanca caducará.

* **Facturar**: Seleccione a continuación si desea que los miembros de la Lista Blanca sean o no facturados. Si desea que sean facturados seleccione **SI** en caso de que no se quiera facturar a los miembros seleccione **NO**.

* **Estado**: Este apartado le permitirá seleccionar el estado de la Lista Blanca. Para listas en uso seleccione el estado **Habilitado**, en caso contrario seleccione **Inhabilitado**.

* **Descripción**: En este campo describa detalladamente el fin con el que ha sido creada la Lista Blanca.

* **Añadir miembro**: Ingrese el **Código**, **CI/RIF** o **Nombre completo de la razón social**. 

Finalmente para guardar la Lista Blanca pulse el botón **Guardar los cambios**.

Solo podrán ser agregados nuevos miembros si ya ha sido guardada la Lista Blanca, en caso contrario el campo **Añadir miembro** estará inhabilitado.

¿Cómo puedo editar una Lista Blanca?
------------------------------------

Dirijase al menú **PROVISIÓN > Lista Blanca** a continuación pulse el botón **Editar** en la parte 
derecha de la Lista Blanca.

.. image:: ./imgs/whitelist/whitelist_edit_button.png

A continuación rellene los campos como se indica en la sección ¿Cómo puedo registrar una nueva Lista Blanca?
y luego pulse el botón **Guardar los cambios**, de esa manera su Lista Blanca será actualizada.

¿Cómo puedo eliminar un miembro de la Lista Blanca?
---------------------------------------------------

Dirijase al menú **PROVISIÓN > Lista Blanca** a continuación pulse el botón **Editar** en la parte 
derecha de la Lista Blanca a la que pertenece el miembro que desea eliminar.

.. image:: ./imgs/whitelist/whitelist_edit_button.png

Esto le permitirá acceder al formulario de la la Lista Blanca a la que pertenece dicho miembro, luego pulse
el botón **Remover este servicio**, ubicado en la parte derecha del miembro al que desea eliminar.

.. image:: ./imgs/whitelist/whitelist_delete_button.png

De esta manera el miembro será eliminado de la Lista Blanca.

¿Cómo puedo transferir un miembro de una Lista Blanca a otra?
-------------------------------------------------------------

Existe una manera de transferir un miembro de una Lista Blanca a otra, para ello Dirijase al menú
**PROVISIÓN > Lista Blanca**, pulse el botón **Editar**, ubicado en el lado derecho de la Lista Blanca en 
la que se encuentra el miembro que desea transferir, luego pulse el botón **Transferir a otra lista**, ubicado
en el lado derecho del miembro que desea transferir.

.. image:: ./imgs/whitelist/whitelist_transfer_button.png

Esto le permitirá acceder al formulario de transferencia de miembros.

.. image:: ./imgs/whitelist/whitelist_transfer_form.png

A continuación detallaremos los campos del formulario:

* **Servicio**: Se trata de una entrada donde se estará definido el código del miembro de la Lista Blanca que será transferido.

* **Lista Blanca**: Este apartado le permitirá seleccionar la Lista Blanca a la que desea transferir el miembro deseado.

Finalmente para transferir el miembro a la Lista Blanca pulse el botón **Transferir**.