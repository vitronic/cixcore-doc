===
CRM
===

El CRM de CixCore permite mantener y registrar historicamente la relación
entre el cliente y la empresa, de tal manera que se pueda en cualquier
momento saber las actividades realizadas sobre, o con un cliente,
también le permitira mantener un catalogo personal de los clientes e
interactuar con ellos, es importante aclarar que en el CRM de CixCore las
cuentas de clientes registradas solo pueden ser vistas por el vendedor
que las registró.

Cuentas
-------

¿Cómo puedo registrar un nuevo cliente?.
----------------------------------------
.. image:: ./imgs/crm/crm-accounts-new-button.png

Dirijase al menú **CRM > Cuentas** a continuación pulse el **botón +** en la esquina
superior derecha, esto le permitira acceder al formulario de registros de
una nueva cuenta cliente, a continuación detallaremos los campos del formulario:

.. image:: ./imgs/crm/crm-accounts-new-form.png

* **Número de identificación**: Se trata de una doble entrada donde se debe definir el tipo de documento y el código, en cuanto al tipo dispone en el menú despegable de siete opciones:

  * **Cédula**: La cédula de la razón social.

  * **Cédula E**: La cédula en el caso de que la razón social sea extranjera.

  * **Natural**: R.I.F de una persona natural.

  * **Juridico**: R.I.F de una persona o ente juridico.

  * **Extranjero**: R.I.F de una persona natural extranjero.

  * **Comuna**: R.I.F de una comuna.

  * **R.I.F**: para entes del gobierno.

En cuanto al código se trata del R.I.F o de la cédula de la razón social sujeta al
formato descrito anteriormente.

* **Nombre completo**: Ingrese el nombre completo de la razón social.

* **Número telefonico**: Se trata de una doble entrada donde se puede ingresar el número telefonico y el recurso, puede ingresar tantos numeros telefonicos como considere necesario.

* **Dirección del correo eléctronico**: Ingrese la dirección del correo eléctronico del contacto, puede ingresar tantos correos eléctronicos como considere necesario.

* **Dirección completa**: Introduzca la dirección fiscal exacta, se utilizara por defecto con propositos de facturación.

* **Grupos**: Seleccione del menú tantos grupos como considere necesario para el registro de la cuenta.

* **Propietarios**: Seleccione de la lista los propietarios que tendrán acceso para gestionar la cuenta, normalmente seleccionese a sí mismo.

* **Seleccione el estado de la cuenta**: Este apartado le permitira seleccionar el estado de la cuenta, para nuevos clientes en etapa de negociación se sugiere utilizar el estado **No definido** o **Pendiente**, una vez que el cliente haya contratado algún servicio se deberá cambiar este estado a **Cliente**, es importante aclarar que solo se puede facturar a una cuenta con estado de Cliente.

* **Seleccione moneda**: Seleccione a continuación la moneda con la que se realizara la facturación de la cuenta.

* **Tratamiento de la cuenta**: Seleccione de la lista el modo en el que será tratado la razón social.

* **Cumpleaños**: Opcionalmente seleccione el cumpleaños o aniversario de la cuenta (Se utilizara con propositos de marketing).

* **Envíar anuncios**: Defina del menú si desea o no envíar anuncios a la dirección de la cuenta (Se utilizara con propositos de marketing).

* **Origen de la cuenta**: Seleccione del menú el origen de la cuenta (Para ser usado con propositos estadisticos).

* **Retención de impuestos**: En el caso de que la razón social sea un agente de retención de impuestos, habilitar esta opción y luego habilitar los impuestos que esta cuenta retiene

.. image:: ./imgs/crm/crm-accounts-new-form-retencion-impuestos.png

* **Adjuntar imagen**: Opcionalmente usted puede adjuntar una imagen que servira para identificar visualmente a la cuenta que está registrando.

Finalmente el **botón envíar** le permitira guardar los datos del formulario.

¿Cómo puedo realizar una cotización?.
-------------------------------------

Dirijase al menú **CRM > Cotizaciones** a continuación pulse el **botón +** en la esquina
superior derecha esto le permitira acceder al formulario de registro de una nueva cotización,
a continuación detallaremos los campos del formulario:

.. image:: ./imgs/crm/cotizacion.png

* **Fecha de emisión**: Esta es la fecha en la que se emite el documento.
* **Fecha de vencimiento**: Esta es la fecha en la que el documento caduca.
* **Vendedor**: El nombre del responsable del documento (Por defecto es quien está registrando el documento).
* **Forma de pago**: Seleccione entre efectivo o deposito bancario.
* **Seleccione la cuenta**: Ingrese el nombre o la cédula de la razon social y a continuación seleccione la cuenta a la cual realizara la cotización.
* **Seleccione un producto o servicio**: Ingrese el código o el nombre de un producto o servicio y a continuación seleccione, a continuación se le presentara un documento como el siguente (Colocar pantallaso aquí) puede seleccionar tantos productos o servicios como sea requerido, puede editar el precio de cada producto o servicio, así como también la cantidad o el descuento.
* **Enviar por correo eléctronico**: Si desea enviar el documento por correo eléctronico seleccione el botón, defina el asunto y el cuerpo del correo eléctronico.

Finalmente para guardar la cotización pulse el botón **Guardar los cambios**

Gestión de cuentas.
-------------------

Existen dos formas para ingresar al perfil de una cuenta y de ese modo gestionar la
misma, una forma es ingresar el R.I.F/Cédula en el buscador principal, seleccionar
la cuenta y pulsar en el botón perfil

.. image:: ./imgs/crm/gestion-cuentas.png

La otra es simplemente ubicar la cuenta en el reporte principal y presionar el boton
perfil en el lado derecho del reporte.

¿Cómo puedo registrar alguna nota sobre un cliente?
---------------------------------------------------

Una vez ingresado al perfil de una cuenta usted puede registrar una nota en la pestaña
**Nota** y pulsando el botón guardar.

.. image:: ./imgs/crm/notas.png

¿Cómo puedo registrar una llamada telefonica de un cliente?
-----------------------------------------------------------

Dirijase a la pestaña **Llamadas**.

.. image:: ./imgs/crm/llamada.png

Seleccione el numero telefonico, a continuación el tipo de llamada, luego el asunto de
la llamada y el detalle de la llamada, finalmente defina el resultado de la llamada y
guarde para realizar el registro, esta llamada quedara registrada con propositos historicos.

¿Cómo puedo enviarle un mensaje SMS a un cliente?
-------------------------------------------------

Dirijase a la pestaña **SMS**, seleccione el numero telefonico, luego seleccione una
importancia y finalmente escriba el mensaje.

.. image:: ./imgs/crm/enviar-sms.png

¿Cómo puedo enviarle un Email a un cliente?
-------------------------------------------

Dirijase a la pestaña **Email**, seleccione uno o varios destinatarios, seleccione una prioridad
del correo, ingrese un asunto y el cuerpo del correo electronico, finalmente pulse enviar E-mail.

.. image:: ./imgs/crm/enviar-email.png

Notificación de pagos
---------------------

Campañas
--------

Cotizaciones
------------

Incidencias
-----------
