============
Guía CixCore
============

.. toctree::
   :maxdepth: 3
   :caption: Contenidos:

   ERP
   SRM
   NOMINA
   CRM
   CONTABLE
   ISP
   PROVISION
   CONFIGURACION
